package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"time"
)

type problem struct {
	q string
	a string
}

func parseLines(lines [][]string) []problem {
	p := make([]problem, len(lines))
	for i, l := range lines {
		p[i] = problem{
			q: l[0],
			a: l[1],
		}
	}
	return p
}

func askProblems(problems []problem, timeLimit int) int {
	timer := time.NewTimer(time.Duration(timeLimit) * time.Second)
	score := 0
problemLoop:
	for i, p := range problems {
		fmt.Printf("Problem #%d: %s = \n", i+1, p.q)
		answerCh := make(chan string)
		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerCh <- answer
		}()

		select {
		case <-timer.C:
			fmt.Println()
			break problemLoop
		case answer := <-answerCh:
			if answer == p.a {
				score++
			}
		}

	}
	return score
}

// PlayGame Main entry point for playing game
func PlayGame() {
	// flag.String (flagName, default value, helpful message as string)
	csvFileName := flag.String("csv", "problems.csv", "a csv file in the format of 'question,answer'")
	timeLimit := flag.Int("timer", 30, "a whole number representing seconds")
	flag.Parse()
	file, err := os.Open(*csvFileName)
	if err != nil {
		fmt.Println("Failed to open the CSV file")
		os.Exit(1)
	}
	fmt.Println("Successfully opened the file", *csvFileName)
	reader := csv.NewReader(file)
	lines, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Failed to read the CSV file")
		os.Exit(1)
	}

	problems := parseLines(lines)
	score := askProblems(problems, *timeLimit)
	fmt.Printf("You scored %d out of %d.\n", score, len(problems))
}
