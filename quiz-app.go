package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"text/template"
)

var templates = template.Must(template.ParseFiles("./templates/home.html", "./templates/play.html"))
var validPath = regexp.MustCompile("^/(home|play|settings)/([a-zA-Z0-9]*)$")

// Page struct for page
type Page struct {
	Title string
	Body  []byte
}

// method for Page struct, p.save()
func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	// ReadFile returns a byte array of the file contents and an error
	// we are ignoring the error in this example
	// body, _ := ioutil.ReadFile(filename)

	// but here we can catch the error and log it
	body, error := ioutil.ReadFile(filename)
	if error != nil {
		return nil, error
	}
	return &Page{Title: title, Body: body}, nil

}

func renderTemplate(w http.ResponseWriter, view string, p *Page) {
	error := templates.ExecuteTemplate(w, view+".html", p)
	if error != nil {
		http.Error(w, error.Error(), http.StatusInternalServerError)
		return
	}
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	renderTemplate(w, "home", nil)
}

func playHandler(w http.ResponseWriter, r *http.Request, title string) {
	renderTemplate(w, "play", nil)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	error := p.save()
	if error != nil {
		http.Error(w, error.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func main() {
	http.HandleFunc("/home/", makeHandler(viewHandler))
	http.HandleFunc("/play/", makeHandler(playHandler))
	// http.HandleFunc("/save/", makeHandler(saveHandler))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
